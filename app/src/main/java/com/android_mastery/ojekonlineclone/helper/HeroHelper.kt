/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.helper

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.telephony.TelephonyManager
import com.android_mastery.ojekonlineclone.base.BaseActivity.Companion.context
import com.android_mastery.ojekonlineclone.model.DataLogin
import java.util.*

fun getDeviceUUID(activity: Activity): String {
    val telephonyManager = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

    if (ActivityCompat.checkSelfPermission(activity.applicationContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_PHONE_STATE), 100)
        return ""
    } else {
        val tmDevice = telephonyManager.deviceId.toString()
        val tmSerial = telephonyManager.simSerialNumber
        val androidID = Settings.Secure.getString(activity.contentResolver, Settings.Secure.ANDROID_ID)

        val deviceUuid = UUID(androidID.hashCode().toLong(),
                tmDevice.hashCode().toLong() shl 32 or tmSerial.hashCode().toLong())

        return deviceUuid.toString()

    }
}

fun savePref(key: String, value: String, context: Context) {
    val sharePreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val editor = sharePreferences.edit()
    editor.putString(key, value)
    editor.apply()
}

fun readPref(key: String, context: Context) :String{
    val sharePreferences = PreferenceManager.getDefaultSharedPreferences(context)
    return sharePreferences.getString(key, "")
}