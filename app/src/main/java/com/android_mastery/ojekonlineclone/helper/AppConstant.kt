/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.helper

class AppConstant{
    object PreferenceKey{
        const val DATA_USER = "data_user"
        const val TOKEN_USER = "token"
        const val ID_USER = "id_user"
        const val NAME_USER = "user_nama"
        const val EMAIL_USER = "user_email"
        const val PASS_USER = "user_password"
        const val HP_USER = "user_hp"
        const val AVATAR_USER = "user_avatar"
        const val REGISTER_USER = "user_register"
        const val GCM_USER = "user_gcm"
        const val LEVEL_USER = "user_level"
        const val STATUS_USER = "user_status"
    }
}