/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.android_mastery.ojekonlineclone.service.googlemaps.GoogleMapsModule
import com.android_mastery.ojekonlineclone.service.webserver.WebServerModule
import com.android_mastery.ojekonlineclone.view.di.AppComponent
import com.android_mastery.ojekonlineclone.view.di.DaggerAppComponent
import java.io.File

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(){
    lateinit var moduleApp :AppComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val cacheFile = File(cacheDir, "responses")
        moduleApp = DaggerAppComponent.builder().googleMapsModule(GoogleMapsModule(cacheFile)).webServerModule(WebServerModule(cacheFile)).build()

        context = applicationContext
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        @get:Synchronized
        var context : Context? = null
        private set
    }
}