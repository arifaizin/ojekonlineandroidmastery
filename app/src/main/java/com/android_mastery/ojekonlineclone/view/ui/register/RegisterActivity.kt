/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.view.ui.register

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.android_mastery.ojekonlineclone.R
import com.android_mastery.ojekonlineclone.base.BaseActivity
import com.android_mastery.ojekonlineclone.service.webserver.WebServerService
import com.android_mastery.ojekonlineclone.view.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import javax.inject.Inject

class RegisterActivity : BaseActivity() {

    @Inject
    lateinit var webServerService: WebServerService

    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        moduleApp.inject(this)
        supportActionBar?.elevation = 0F

        viewModel = ViewModelProviders.of(this@RegisterActivity, RegisterViewModelFactory(webServerService)).get(RegisterViewModel::class.java)

        viewModel.getRegisterState().observe(this@RegisterActivity, Observer { state ->
            when(state){
                is RegisterState.EmailEmpty -> edEmail.error = "Masih kosong"
                is RegisterState.PassEmpty -> edPassword.error = "Masih kosong"
                is RegisterState.PhoneEmpty -> edNomorHP.error = "Masih kosong"
                is RegisterState.NameEmpty -> edNama.error = "Masih kosong"
                is RegisterState.PostRegisterRequest -> viewModel.sendRegister(email = state.email, phone = state.phone, pass = state.pass, nama = state.nama)
                is RegisterState.GetSuccessRegister->{
                    toast(state.msg)
                    finish()
                    startActivity<LoginActivity>()
                }
                is RegisterState.GetFailedRegister->{
                    toast(state.error)
                }
            }
        })

        btnRegister.onClick {
            viewModel.inputRegister(edtEmail = edEmail, edtName = edNama, edtPass = edPassword, edtPhone = edNomorHP)
        }

    }
}
