/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.view.di

import com.android_mastery.ojekonlineclone.service.googlemaps.GoogleMapsModule
import com.android_mastery.ojekonlineclone.service.googlemaps.GoogleMapsService
import com.android_mastery.ojekonlineclone.service.webserver.WebServerModule
import com.android_mastery.ojekonlineclone.service.webserver.WebServerService
import com.android_mastery.ojekonlineclone.view.ui.login.LoginActivity
import com.android_mastery.ojekonlineclone.view.ui.register.RegisterActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [GoogleMapsModule::class, WebServerModule::class])
interface AppComponent {
    fun googleMapsService(): GoogleMapsService
    fun webServerService(): WebServerService
    fun inject(registerActivity: RegisterActivity)
    fun inject(loginActivity: LoginActivity)
}