/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.view.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.android_mastery.ojekonlineclone.R
import com.android_mastery.ojekonlineclone.helper.AppConstant
import com.android_mastery.ojekonlineclone.helper.readPref
import com.android_mastery.ojekonlineclone.view.ui.login.LoginActivity
import com.android_mastery.ojekonlineclone.view.ui.main.MainActivity
import org.jetbrains.anko.startActivity

class SplashActivity : AppCompatActivity() {

    private lateinit var delayHandler: Handler

    private val SPLASH_DELAY: Long = 3000

    private val runnable: Runnable = Runnable {
        val email = readPref(AppConstant.PreferenceKey.EMAIL_USER, this@SplashActivity)
        val idUser = readPref(AppConstant.PreferenceKey.ID_USER, this@SplashActivity)
        val token = readPref(AppConstant.PreferenceKey.TOKEN_USER, this@SplashActivity)

        when{
            email.isEmpty() || idUser.isEmpty() || token.isEmpty() -> {
                startActivity<LoginActivity>()
                finish()
            }
            else -> {
                if (!isFinishing) {
                    startActivity<MainActivity>()
                    finish()
                }
            }
        }



    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        delayHandler = Handler()

        delayHandler.postDelayed(runnable, SPLASH_DELAY)


    }

    override fun onDestroy() {
        if (delayHandler != null) {
            delayHandler.removeCallbacks(runnable)
        }
        super.onDestroy()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                )
    }
}
