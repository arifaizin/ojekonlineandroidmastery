/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.view.ui.login

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.widget.EditText
import com.android_mastery.ojekonlineclone.base.BaseViewModel
import com.android_mastery.ojekonlineclone.model.DataLogin
import com.android_mastery.ojekonlineclone.model.LoginResponse
import com.android_mastery.ojekonlineclone.service.webserver.GetLoginCallback
import com.android_mastery.ojekonlineclone.service.webserver.WebServerService


interface LoginContract {
    fun getLoginState(): MutableLiveData<LoginState>
    fun inputLogin(device: String, edtEmail: EditText, edtPass: EditText)
    fun sendLogin(device: String, email: String, pass: String)
}

sealed class LoginState {
    object ShowLoading : LoginState()
    object DismissLoading : LoginState()
    object EmailEmpty : LoginState()
    object DeviceEmpty : LoginState()
    object PassEmpty : LoginState()
    data class PostLoginRequest(val device: String, val email: String, val pass: String) : LoginState()
    data class GetSuccessLogin(val loginResponse: LoginResponse) : LoginState()
    data class GetFailedLogin(val error: String) : LoginState()
}

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(private val webServerService: WebServerService) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(webServerService) as T
    }
}

class LoginViewModel(val serverService: WebServerService) : BaseViewModel(), LoginContract {
    val state: MutableLiveData<LoginState> = MutableLiveData()

    override fun getLoginState(): MutableLiveData<LoginState> = state

    override fun inputLogin(device: String, edtEmail: EditText, edtPass: EditText) {
        when {
            edtEmail.text.toString().isEmpty() -> state.value = LoginState.EmailEmpty
            device.isEmpty() -> state.value = LoginState.DeviceEmpty
            edtPass.text.toString().isEmpty() -> state.value = LoginState.PassEmpty
            else -> state.value = LoginState.PostLoginRequest(device, edtEmail.text.toString(), edtPass.text.toString())
        }
    }

    override fun sendLogin(device: String, email: String, pass: String) {
        val subscription = serverService.loginCustomer(object : GetLoginCallback {
            override fun onProgress() {
                state.value = LoginState.ShowLoading
            }

            override fun onSuccess(loginResponse: LoginResponse) {
                state.value = LoginState.DismissLoading
                state.value = LoginState.GetSuccessLogin(loginResponse)
            }

            override fun onError(msg: String) {
                state.value = LoginState.DismissLoading
                state.value = LoginState.GetFailedLogin(msg)
            }

        }, device = device, email = email, pass = pass)
        addDisposable(subscription)
    }

}