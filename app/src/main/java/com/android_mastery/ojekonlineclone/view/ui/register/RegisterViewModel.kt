/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.view.ui.register

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.widget.EditText
import com.android_mastery.ojekonlineclone.base.BaseViewModel
import com.android_mastery.ojekonlineclone.service.webserver.GetRegisterCallback
import com.android_mastery.ojekonlineclone.service.webserver.WebServerService

interface RegisterContract {
    fun getRegisterState(): MutableLiveData<RegisterState>
    fun inputRegister(edtEmail: EditText, edtName: EditText, edtPass: EditText, edtPhone: EditText)
    fun sendRegister(email: String, nama: String, pass: String, phone: String)
}

sealed class RegisterState {
    object ShowLoading : RegisterState()
    object DismissLoading : RegisterState()
    object EmailEmpty : RegisterState()
    object NameEmpty : RegisterState()
    object PassEmpty : RegisterState()
    object PhoneEmpty : RegisterState()
    data class PostRegisterRequest(val email: String, val nama: String, val pass: String, val phone: String): RegisterState()
    data class GetSuccessRegister(val msg: String): RegisterState()
    data class GetFailedRegister(val error: String): RegisterState()
}

@Suppress("UNCHECKED_CAST")
class RegisterViewModelFactory(private val webServerService: WebServerService) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterViewModel(webServerService) as T
    }
}

class RegisterViewModel(val serverService: WebServerService) : BaseViewModel(), RegisterContract {
    val state: MutableLiveData<RegisterState> = MutableLiveData()

    override fun getRegisterState(): MutableLiveData<RegisterState> = state

    override fun inputRegister(edtEmail: EditText, edtName: EditText, edtPass: EditText, edtPhone: EditText) {
        when {
            edtEmail.text.toString().isEmpty() -> state.value = RegisterState.EmailEmpty
            edtName.text.toString().isEmpty() -> state.value = RegisterState.NameEmpty
            edtPhone.text.toString().isEmpty() -> state.value = RegisterState.PhoneEmpty
            edtPass.text.toString().isEmpty() -> state.value = RegisterState.PassEmpty
            else -> state.value = RegisterState.PostRegisterRequest(edtEmail.text.toString(), edtName.text.toString(), edtPass.text.toString(), edtPhone.text.toString())
        }
    }

    override fun sendRegister(email: String, nama: String, pass: String, phone: String) {
        val subscription = serverService.registerCustomer(object : GetRegisterCallback{
            override fun onProgress() {
              state.value = RegisterState.ShowLoading
            }

            override fun onSuccess(msg: String) {
                state.value = RegisterState.DismissLoading
                state.value = RegisterState.GetSuccessRegister(msg)
            }

            override fun onError(msg: String) {
                state.value = RegisterState.DismissLoading
                state.value = RegisterState.GetFailedRegister(msg)
            }

        }, nama = nama, email = email, pass = pass, phone = phone)
        addDisposable(subscription)
    }

}