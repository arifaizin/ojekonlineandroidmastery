/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.view.ui.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.android_mastery.ojekonlineclone.R
import com.android_mastery.ojekonlineclone.base.BaseActivity
import com.android_mastery.ojekonlineclone.helper.AppConstant.PreferenceKey.EMAIL_USER
import com.android_mastery.ojekonlineclone.helper.AppConstant.PreferenceKey.ID_USER
import com.android_mastery.ojekonlineclone.helper.AppConstant.PreferenceKey.TOKEN_USER
import com.android_mastery.ojekonlineclone.helper.getDeviceUUID
import com.android_mastery.ojekonlineclone.helper.savePref
import com.android_mastery.ojekonlineclone.service.webserver.WebServerService
import com.android_mastery.ojekonlineclone.view.ui.main.MainActivity
import com.android_mastery.ojekonlineclone.view.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    @Inject
    lateinit var webServerService: WebServerService

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        moduleApp.inject(this)
        tvToRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        viewModel = ViewModelProviders.of(this@LoginActivity, LoginViewModelFactory(webServerService)).get(LoginViewModel::class.java)

        viewModel.getLoginState().observe(this@LoginActivity, Observer { state ->
            when (state) {
                is LoginState.EmailEmpty -> edEmail.error = "Masih kosong"
                is LoginState.PassEmpty -> edPassword.error = "Masih kosong"
                is LoginState.DeviceEmpty -> toast("Device UUID Masih kosong")
                is LoginState.PostLoginRequest -> viewModel.sendLogin(email = state.email, device = state.device, pass = state.pass)
                is LoginState.GetSuccessLogin -> {
                    savePref(TOKEN_USER, state.loginResponse.token, this@LoginActivity)
                    savePref(EMAIL_USER, state.loginResponse.data.user_email, this@LoginActivity)
                    savePref(ID_USER, state.loginResponse.data.id_user, this@LoginActivity)
                    toast(state.loginResponse.msg)
                    startActivity<MainActivity>()
                    finish()
                }
                is LoginState.GetFailedLogin -> {
                    toast(state.error)
                }
            }
        })

        btnLogin.onClick {
            viewModel.inputLogin(getDeviceUUID(this@LoginActivity), edEmail, edPassword)
        }
    }
}
