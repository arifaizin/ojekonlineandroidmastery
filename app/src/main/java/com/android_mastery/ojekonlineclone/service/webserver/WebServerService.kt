/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.service.webserver

import com.android_mastery.ojekonlineclone.model.DataLogin
import com.android_mastery.ojekonlineclone.model.LoginResponse
import rx.Scheduler
import rx.Single
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class WebServerService internal constructor(private val webServerApi: WebServerApi) {
    fun registerCustomer(callback:GetRegisterCallback, nama:String, email:String, pass:String, phone:String):Subscription{
        return webServerApi.register(nama,email,pass,phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext{throwable  -> Single.error(throwable)}
                .doOnSubscribe { callback.onProgress() }
                .subscribe ({
                    when{
                        it.result.equals("true", true) -> callback.onSuccess(it.msg)
                        else -> callback.onError(it.msg)
                    }
                },{
                    callback.onError(it.message.toString())
                })
    }

    fun loginCustomer(callback:GetLoginCallback, device:String, email:String, pass:String):Subscription{
        return webServerApi.login(device,email,pass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext{throwable  -> Single.error(throwable)}
                .doOnSubscribe { callback.onProgress() }
                .subscribe ({
                    when{
                        it.result.equals("true", true) -> callback.onSuccess(it)
                        else -> callback.onError(it.msg)
                    }
                },{
                    callback.onError(it.message.toString())
                })
    }
}

interface GetRegisterCallback {
    fun onProgress()
    fun onSuccess(msg: String)
    fun onError(msg: String)
}

interface GetLoginCallback {
    fun onProgress()
    fun onSuccess(loginResponse: LoginResponse)
    fun onError(msg: String)
}
