/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.service.googlemaps

import com.android_mastery.ojekonlineclone.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import javax.inject.Named
import javax.inject.Singleton

@Module
class GoogleMapsModule(var cahceFile: File) {

    @Provides
    @Named("provideGoogleMaps")
    @Singleton
    internal fun provideGoogleMaps(): Retrofit {
        var cahce: Cache? = null
        try {
            cahce = Cache(cahceFile, (10 * 1024 * 1024).toLong())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .cache(cahce)
                .build()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.MAPS_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideGoogleMapsApi(@Named ("provideGoogleMaps") retrofit: Retrofit):GoogleMapsApi{
        return retrofit.create(GoogleMapsApi::class.java)
    }

    @Provides
    @Singleton
    fun provideGoogleMapsService(googleMapsApi: GoogleMapsApi):GoogleMapsService{
        return GoogleMapsService(googleMapsApi)
    }
}