/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.service.webserver

import com.android_mastery.ojekonlineclone.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import javax.inject.Named
import javax.inject.Singleton


@Module
class WebServerModule(var cahceFile: File) {

    @Provides
    @Named("provideWebServer")
    @Singleton
    internal fun provideWebServer(): Retrofit {
        var cahce: Cache? = null
        try {
            cahce = Cache(cahceFile, (10 * 1024 * 1024).toLong())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                .cache(cahce)
                .build()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideWebServer(@Named("provideWebServer") retrofit: Retrofit): WebServerApi {
        return retrofit.create(WebServerApi::class.java)
    }

    @Provides
    @Singleton
    fun provideWebServerService(webServerApi: WebServerApi): WebServerService {
        return WebServerService(webServerApi)
    }
}