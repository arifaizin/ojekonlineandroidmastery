/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.service.webserver

import com.android_mastery.ojekonlineclone.model.BaseResponse
import com.android_mastery.ojekonlineclone.model.LoginResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Single


interface WebServerApi {

    @FormUrlEncoded
    @POST("daftar")
    fun register(@Field("nama") nama: String,
                 @Field("email") email: String,
                 @Field("password") password: String,
                 @Field("phone") phone: String): Single<BaseResponse>



    @FormUrlEncoded
    @POST("login")
    fun login(@Field("device") device: String,
                 @Field("f_email") f_email: String,
                 @Field("f_password") f_password: String): Single<LoginResponse>
}