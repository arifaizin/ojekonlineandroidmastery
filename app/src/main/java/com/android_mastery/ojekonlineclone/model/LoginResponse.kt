/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

class LoginResponse(
        @SerializedName("token") val token :String = "",
        @SerializedName("idUser") val idUser :String = "",
        @SerializedName("data") val data :DataLogin
        ) : BaseResponse()

@Parcelize
data class DataLogin (
        @SerializedName("id_user") val id_user :String = "",
        @SerializedName("user_nama") val user_nama :String = "",
        @SerializedName("user_email") val user_email :String = "",
        @SerializedName("user_password") val user_password :String = "",
        @SerializedName("user_hp") val user_hp :String = "",
        @SerializedName("user_avatar") val user_avatar :String = "",
        @SerializedName("user_gcm") val user_gcm :String = "",
        @SerializedName("user_register") val user_register :String = "",
        @SerializedName("user_level") val user_level :String = "",
        @SerializedName("user_status") val user_status :String = ""
) :Parcelable