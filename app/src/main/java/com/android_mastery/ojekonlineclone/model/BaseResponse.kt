/*
 * Copyright (c) 2018.
 * Gilang Ramadhan (gilang@imastudio.co.id)
 */

package com.android_mastery.ojekonlineclone.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


open class BaseResponse(@SerializedName("result")
                        val result : String = "",
                        @SerializedName("msg")
                        val msg : String = "")